
MyAccount="tkaji"
DSID="514873"
nEvtPerFile=5000
nEvtPerJob=200

TRF="Sim_tf.py --inputEVNTFile=%IN --outputHITSFile=%OUT.HITS.pool.root --runNumber=${DSID} --firstEvent %FIRSTEVENT:0 --skipEvents %SKIPEVENTS --maxEvents=${nEvtPerJob} --randomSeed %RNDM:0 --simulator 'FullG4MT_QS' --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' --preInclude 'EVNTtoHITS:Campaigns.MC23SimulationSingleIoV' --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' --postInclude 'default:PyJobTransforms.UseFrontier' --CA 'all:True' --multithreaded True --truthStrategy 'MC12LLP'"
#--athenaopts='--threads=1' 

inDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.EVNT.20240605_1_EXT0"
outDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.HITS.20240611_1"
pathena --inDS ${inDS} --outDS ${outDS} --nEventsPerFile ${nEvtPerFile} --nEventsPerJob ${nEvtPerJob} --trf="${TRF}"

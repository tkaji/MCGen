
MyAccount="tkaji"
DSID="999999"
nFile=100
nEvtPerFile=5000

TRF="Sim_tf.py --inputEVNTFile=%IN --outputHITSFile=%OUT.HITS.pool.root --runNumber=${DSID} --firstEvent %FIRSTEVENT:0 --skipEvents %SKIPEVENTS --maxEvents=${nEvtPerFile} --randomSeed %RNDM:0 --simulator 'FullG4MT_QS' --conditionsTag 'default:OFLCOND-MC21-SDR-RUN3-07' --preInclude \"EVNTtoHITS:Campaigns.MC23SimulationSingleIoV\" --geometryVersion \"default:ATLAS-R3S-2021-03-02-00\" --postInclude \"default:PyJobTransforms.UseFrontier\" --CA \"all:True\" --multithreaded True"

inDS="user.tkaji.MCGen.999999.single_proton_Eta0_Pt100_5000MeV.20240605_1_EXT0"
outDS="user.tkaji.MCGen.999999.single_proton_Eta0_Pt100_5000MeV.HITS.20240605_2"
pathena --inDS ${inDS} --outDS ${outDS} --nEventsPerFile ${nEvtPerFile} --nEventsPerJob ${nEvtPerFile} --trf="${TRF}"

# Produce DAOD_LLP1 with PixelClusters
How to build custom athena
```
setupATLAS
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/tkaji/athena.git -p DerivationFrameworkLLP
cd athena
git checkout bDAOD_LLP1_PixelClus
mkdir ../build
cd ../build
source ../asetup.sh
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j
source x86_64-el9-*/setup.sh
```

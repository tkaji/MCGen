MyAccount="tkaji"
DSID="514873"

#TRF="Reco_tf.py --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --runNumber=${DSID} --skipEvents %SKIPEVENTS --maxEvents=-1 --digiSeedOffset1 %RNDM:0 --digiSeedOffset2 %RNDM:1 --autoConfiguration 'everything' --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-02' --preInclude 'all:Campaigns.MC23NoPileUp' --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' --postInclude 'default:PyJobTransforms.UseFrontier' --CA 'default:True' --multithreaded True --preExec 'all:flags.Tracking.writeExtendedSi_PRDInfo=True;flags.Tracking.writeExtendedTRT_PRDInfo=True'"

TRF="Derivation_tf.py --inputAODFile %IN --outputDAODFile %OUT.DAOD_LLP1.pool.root --CA 'all:True' --athenaMPMergeTargetSize 'DAOD_*:0' --formats 'LLP1' --multiprocess 'True' --multithreadedFileValidation 'True' --passThrough 'Derivation:True' --postExec 'default:if ConfigFlags.Concurrency.NumProcs>0: cfg.getService(\"AthMpEvtLoopMgr\").ExecAtPreFork=[\"AthCondSeq\"];' --preExec 'default:flags.Derivation.LLP.saveFullTruth=True;flags.Derivation.LLP.doTrackSystematics=True;' --sharedWriter 'True' --passThrough 'True'"


inDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.AOD.20240617_2_EXT0"
outDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.PixelCluster.DAOD_LLP1.20240618_1"
pathena --inDS ${inDS} --outDS ${outDS} --nFilesPerJob 10 --trf="${TRF}" --nCore 8


# *************************** This File is not validated yet !! ***************************

MyAccount="tkaji"
DSID="514873"

TRF="Reco_tf.py --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --runNumber=${DSID} --skipEvents %SKIPEVENTS --maxEvents=-1 --digiSeedOffset1 %RNDM:0 --digiSeedOffset2 %RNDM:1 --autoConfiguration 'everything' --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-02' --preInclude 'all:Campaigns.MC23NoPileUp' --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' --postInclude 'default:PyJobTransforms.UseFrontier' --CA 'default:True' --multithreaded True --preExec 'all:flags.Tracking.writeExtendedSi_PRDInfo=True;flags.Tracking.writeExtendedTRT_PRDInfo=True'"
#TRF="Reco_tf.py --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --runNumber=${DSID} --skipEvents %SKIPEVENTS --maxEvents=-1 --digiSeedOffset1 %RNDM:0 --digiSeedOffset2 %RNDM:1 --autoConfiguration 'everything' --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-02' --preInclude 'all:Campaigns.MC23NoPileUp' 'RDOtoRDOTrigger:TrigInDetConfig/IDTrig_MC23a_preInclude.py' --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' --postInclude 'default:PyJobTransforms.UseFrontier' --CA 'default:True' 'RDOtoRDOTrigger:False' --multithreaded True --steering 'doRDO_TRIG' 'doTRIGtoALL' --preExec 'all:flags.Tracking.writeExtendedSi_PRDInfo=True;flags.Tracking.writeExtendedTRT_PRDInfo=True'"
#--athenaopts='--threads=1' 

inDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.HITS.20240611_1_EXT0"
outDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.AOD.20240617_2"
pathena --inDS ${inDS} --outDS ${outDS} --nFilesPerJob 1 --trf="${TRF}" --nCore 8

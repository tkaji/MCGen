#asetup Athena,23.0.20

MyAccount="tkaji"
DSID="999999"
nEvtPerFile=5000

TRF="Reco_tf.py --inputHITSFile %IN --outputAODFile %OUT.AOD.pool.root  --CA 'default:True' --autoConfiguration 'everything' --conditionsTag 'default:OFLCOND-MC23-SDR-RUN3-02' --geometryVersion 'default:ATLAS-R3S-2021-03-02-00' --multithreaded 'True' --postInclude 'default:PyJobTransforms.UseFrontier' --preInclude 'all:Campaigns.MC23NoPileUp' --preExec 'all:flags.Tracking.writeExtendedSi_PRDInfo=True;flags.Tracking.writeExtendedTRT_PRDInfo=True'"

inDS="user.tkaji.MCGen.999999.single_proton_Eta0_Pt100_5000MeV.HITS.20240605_2_EXT0"
outDS="user.tkaji.MCGen.999999.single_proton_Eta0_Pt100_5000MeV.AOD.20240617_1"
pathena --inDS ${inDS} --outDS ${outDS} --nEventsPerFile ${nEvtPerFile} --nEventsPerJob ${nEvtPerFile} --trf="${TRF}" --nCore 8

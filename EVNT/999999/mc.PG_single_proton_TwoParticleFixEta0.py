evgenConfig.description = "Two protons"
evgenConfig.keywords = ["singleParticle", "proton"]
evgenConfig.contact = ["toshiaki.kaji@cern.ch"] 
evgenConfig.generators += ["ParticleGun"]
#evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
import math

class MyParticleSampler(PG.ParticleSampler):
    "A special sampler with two _correlated_ particles."

    def __init__(self):
        self.mom1 = PG.PtEtaMPhiSampler(pt=PG.LogSampler(100., 5000.), eta=0, phi=[0, math.pi])

    def shoot(self):
        "Return a vector of sampled particles"
        p1 = PG.SampledParticle(2212, self.mom1.shoot())
        pt1  = p1.mom.Pt()
        eta1 = p1.mom.Eta()
        phi1 = p1.mom.Phi()

        mom2 = PG.PtEtaMPhiSampler(pt=pt1,
                                   eta=0,
                                   phi=phi1+math.pi)
        p2 = PG.SampledParticle(-2212, mom2.shoot())
        return [p1, p2]

topSeq += PG.ParticleGun()
topSeq.ParticleGun.randomSeed = 123456
topSeq.ParticleGun.sampler = MyParticleSampler()
topSeq.ParticleGun.sampler.pos = PG.PosSampler(x=[-0.,0.], y=[-0.,0.], z=[-100.,100.])

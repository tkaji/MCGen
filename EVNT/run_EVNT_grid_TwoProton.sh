
MyAccount="tkaji"
DSID="999999"
#JobConfig="mc.PG_single_proton_TwoParticleFixEta0.py"
nFile=100
nEvtPerFile=5000

outDS="user.${MyAccount}.MCGen.${DSID}.single_proton_Eta0_Pt100_5000MeV.20240605_1"
TRF="Gen_tf.py --ecmEnergy=13600 --jobConfig=${DSID} --firstEvent %SKIPEVENTS --randomSeed=%RNDM:0 --maxEvents=${nEvtPerFile} --outputEVNTFile=%OUT.EVNT.pool.root"

pathena --outDS ${outDS} --split ${nFile} --nEventsPerJob=${nEvtPerFile} --trf="${TRF}"


https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware

https://bigpanda.cern.ch/task/29924490/
https://its.cern.ch/jira/browse/ATLPHYSVAL-1040
https://bigpanda.cern.ch/task/39512063/
https://bigpanda.cern.ch/task/39512068/
https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/47678/

For charginos (Old release with 13 TeV)
```
setupATLAS -c centos7 -m /gpfs:/gpfs
asetup AthGeneration,21.6.92
```

For single particle gun
```
setupATLAS -c centos7 -m /gpfs:/gpfs
asetup AthGeneration,23.6.32
```

## Problems for chargino production
### Newer releases
```
Pythia8              INFO found LHEF version 3
Pythia8             ERROR Something wrong when building list of weight names: 149 vs 111, exiting ...
Pythia8             ERROR Pythia8_i/src/Pythia8_i.cxx:459 (StatusCode Pythia8_i::fillEvt(HepMC::GenEvent*)): code FAILURE: fillWeights(evt)
Pythia8             ERROR GeneratorModules/src/GenModule.cxx:90 (StatusCode GenModule::execute()): code FAILURE: this->fillEvt(evt)
Pythia8             ERROR Maximum number of errors ( 'ErrorMax':1) reached.
```

### Old releases with 13.6TeV
This case, we can obtain output file.
But it is treated as a failed job due to an error.

```
EvgenFixSeq          INFO Finalizing EvgenFixSeq...
EvgenPreFilterSeq    INFO Finalizing EvgenPreFilterSeq...
TestHepMC            INFO Events passed = 22, Events Failed = 5
TestHepMC            INFO  Event rate with invalid Beam Particles = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with beam particles and status not equal to 4 = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with incorrect beam particle energies = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with zero vertices = 0
TestHepMC            INFO  Event rate with NaN (Not A Number) or inf found in the event record vertex positions = 0%
TestHepMC            INFO  Event rate with vertices displaced more than 100~mm in transverse direction for particles with status code other than 1 and 2 = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with vertices displaced more than 1000~mm = 18.5185%
TestHepMC            INFO  Event rate with NAN (Not A Number) or inf found in particle momentum values = 0%
TestHepMC            INFO  Event rate with undecayed pi0's with status 1 or 2 = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with unstable particles with no end vertex = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with negative total energy like for tachyonic particles = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with particles with improper decay properties = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with undisplaced daughters of long lived hadrons = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with non zero photon mass = 0% (not included in test efficiency)
TestHepMC            INFO  Event rate with no energy balance = 0%
TestHepMC            INFO  Event rate with no momentum balance = 0%
TestHepMC            INFO  Event rate with negative energy particles = 0%
TestHepMC            INFO  Event rate with tachyons = 0%
TestHepMC            INFO  Event rate with stable or unstable particles with no parents = 0%
TestHepMC            INFO  Event rate with unstable particle with no decay vertex = 0%
TestHepMC            INFO  Event rate with undecayed Pi0's = 0%
TestHepMC            INFO  Event rate with undisplaced decay daughters of displaced vertices = 0%
TestHepMC            INFO  Event rate with particles with status 1 but lifetime < 0.00333333~ns = 0%
TestHepMC            INFO  Event rate with energy sum of interacting particles non known by Geant4 above 100 MeV = 0%
TestHepMC            INFO  Event rate with unknown PDG IDs 0%
TestHepMC            INFO Efficiency = 81.4815%
TestHepMC           FATAL EFFICIENCY IS TOO LOW! 81.4815% found, but at least: 98% required
EvgenTestSeq         INFO Finalizing EvgenTestSeq...
EvgenFilterSeq       INFO Finalizing EvgenFilterSeq...
MissingEtFilter      INFO Events passed = 10  Events failed = 12
MissingEtFilter...   INFO Events passed = 0  Events failed = 10
Py:EvgenFilterSeq    INFO Filter Expression = (MissingEtFilter and not MissingEtFilterUpperCut)
Py:EvgenFilterSeq    INFO Filter Efficiency = 0.454545 [10 / 22]
Py:EvgenFilterSeq    INFO Weighted Filter Efficiency = 0.460778 [0.019450 / 0.042211]
```

This error should be avoidable because we can produce samples with lifetime=10 ns.
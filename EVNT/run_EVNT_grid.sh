
MyAccount="tkaji"
DSID="514873"
maxEvents=5000

outDS="user.${MyAccount}.MCGen13TeV.${DSID}.Wino800GeV_CC_MET60.EVNT.20240605_1"
TRF="Gen_tf.py --outputEVNTFile=%OUT.${DSID}.EVNT.pool.root --ecmEnergy=13000 --jobConfig=${DSID} --firstEvent=%SKIPEVENTS --randomSeed=%RNDM:0 --maxEvents=${maxEvents}"

# (Total number of events) = (split) * (nEventsPerJob)
pathena --outDS ${outDS} --split 2 --nEventsPerJob=${maxEvents} --trf="${TRF}"

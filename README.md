# MCGen

https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroup

## EVNT
Currently, old production release with 13 TeV

## HITS (EVNT to HITS)

## AOD (MC:HITS to AOD)
Custom AOD with PixelClusters without trigger information

## DAOD_LLP1 (MC:AOD to DAOD_LLP1)
Custom DAOD_LLP1 with PixelClusters